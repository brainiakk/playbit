<?php

namespace App\Http\Controllers;

use App\Mail\ActivationMail;
use App\Models\ActivationCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ActivationCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendCode(Request $request){
        if($request->email != ""){
            $user = User::whereEmail($request->email)->first();
            if($user){
                if($request->ajax()){
                    return response()->json(['status' => 'error', 'msg' => 'Error!! User with this Email address already exists.'], 422);
                }
                Session::flash('error', "Error!! User with this Email address already exists.");
                return Redirect::back();
            }else{
                $coded = ActivationCode::whereEmail($request->email)->first();
                $code = rand(1111, 9999);
                if($coded){
                    $code_save = ActivationCode::whereEmail($request->email)->update(['code' => $code, 'expires_at' => Carbon::now()->addMinutes(15)]);
                }else{
                    $activ_code = new ActivationCode();
                    $activ_code->email = $request->email;
                    $activ_code->expires_at = Carbon::now()->addMinutes(15);
                    $activ_code->code = $code;
                    $code_save =  $activ_code->save();
                }
                if($code_save){
                    $email = $request->email;
                    Mail::to($email)->send(new ActivationMail($code, $email));
                    if (count(Mail::failures()) > 0){

                        if($request->ajax()){
                            return response()->json(['status' => 'error', 'msg' => 'Email was not sent!!'], 422);
                        }

                        Session::flash('error', "Oops!! Verification code was not sent.");
                        return Redirect::back();
                    } else {
                        if($request->ajax()){
                            return response()->json(['status' => 'success', 'msg' => 'Verification code has been sent successfully to your email address!!'], 200);
                        }

                        Session::flash('success', "Verification code was sent successfully!!");
                        return Redirect::back();
                    }
                }else{
                    if($request->ajax()){
                        return response()->json(['status' => 'error', 'msg' => 'Oops!! Something went wrong.'], 422);
                    }

                    Session::flash('error', "Oops!! Something went wrong.");
                    return Redirect::back();
                }
            }
        }
        if($request->ajax()){
            return response()->json(['status' => 'error', 'msg' => 'Error!! please provide a valid Email address.'], 422);
        }

        Session::flash('error', "Oops!! Something went wrong.");
        return Redirect::back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function show(ActivationCode $activationCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivationCode $activationCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivationCode $activationCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivationCode  $activationCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivationCode $activationCode)
    {
        //
    }
}
