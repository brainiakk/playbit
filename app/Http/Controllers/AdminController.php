<?php

namespace App\Http\Controllers;

use App\Models\Audio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.admin.index');
    }

    public function allAudio()
    {
        $audios = Audio::owner()->latest()->paginate(20);
        return view('portal.admin.audio.index', compact('audios'));
    }
    public function pending()
    {
        $audios = Audio::pending()->latest()->paginate(20);
        return view('portal.admin.audio.pending', compact('audios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Audio $audio)
    {
        return view('portal.admin.audio.create', ['audio' => $audio]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function show(Audio $audio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function edit(Audio $audio)
    {
        return view('portal.admin.audio.create', ['audio' => $audio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Audio $audio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function publish(Audio $audio)
    {
        if (\Auth::user()->isAdmin()) {
            $audio->status = '2';
            if ($audio->save()) {
                Session::flash('success', "Audio File Published Successfully!!");
                return Redirect::back();
            } else {
                Session::flash('error', "Audio File Failed to Publish!!");
                return Redirect::back();
            }
        }
        Session::flash('error', "Access Denied!!");
        return Redirect::route('home');
    }
    public function reject(Audio $audio)
    {
        if (\Auth::user()->isAdmin()) {
            $audio->status = '0';
            if ($audio->save()) {
                Session::flash('success', "Audio File Rejected Successfully!!");
                return Redirect::back();
            } else {
                Session::flash('error', "Audio File Rejected Failed!!");
                return Redirect::back();
            }
        }
        Session::flash('error', "Access Denied!!");
        return Redirect::route('home');
    }
}
