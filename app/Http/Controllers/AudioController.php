<?php

namespace App\Http\Controllers;

use App\Models\Audio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Owenoj\LaravelGetId3\GetId3;
use wapmorgan\MediaFile\MediaFile;
use wapmorgan\Mp3Info\Mp3Info;
class AudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function audios(Request $request)
    {
        $audios = Audio::active()->latest()->with('user')->get();
        return response()->json($audios, 200);
    }
    public function getDownload(Audio $audio){

//        $file = public_path().;
        return Response::download($audio->file);
    }
    public function home()
    {
        $audios = Audio::active()->latest()->get();
        return view('pages.index', compact('audios'));
    }
    public function index()
    {
        return view('portal.user.index');
    }

    public function all()
    {
        if (\Auth::user()->isAdmin()){
            $audios = Audio::latest()->paginate(20);
        }else {
            $audios = Audio::owner()->latest()->paginate(20);
        }
        return view('portal.user.audio.index', ['audios'=> $audios]);
    }


    public function pending()
    {
        if (\Auth::user()->isAdmin()){
            $audios = Audio::latest()->pending()->paginate(20);
        }else {
            $audios = Audio::owner()->pending()->latest()->paginate(20);
        }
        return view('portal.user.audio.pending', ['audios'=> $audios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Audio $audio)
    {
        return view('portal.user.audio.create', ['audio' => $audio]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'audio_file' => ['required', 'string'],
            'cover' => ['required', 'string'],
            'caption' => ['required', 'string', 'min:3'],
        ], [
            'audio_file.required' => "Please select an audio file.",
        ]);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->errors());
        }

        $audio = Audio::create([
            'user_id' => \Auth::id(),
            'caption' => $request->caption,
            'file' => $request->audio_file,
            'image' => $request->cover,
            'duration' => $request->duration,
            'status' => $request->status,
        ]);
        if ($audio){
            Session::flash('success', "Audio Shared Successfully!!");
            return Redirect::back();
        } else {
            Session::flash('error', "Audio Shared Failed!!");
            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function show(Audio $audio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function edit(Audio $audio)
    {
        if ($audio->isOwner()) {
            return view('portal.user.audio.create', ['audio' => $audio]);
        }
        Session::flash('error', "Access Denied!!");
        return Redirect::route('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function uploadAudio(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'audio' => ['required', 'max:10212'],
        ], [
            'audio.required' => "Please select an audio file.",
        ]);
        if($validator->fails()){
            return response(['errors' => $validator->errors()], 422);
        }
        if ($request->hasfile('audio')) {
            $time = Carbon::now();
            $file = $request->file('audio');
            $track = GetId3::fromUploadedFile($request->file('audio'));
            $duration = $track->getPlaytime();
            $extension = $track->getFileFormat();
            $ext_array = ['mp3','ogg','wav','mp4','wma','aac'];
            if(!in_array($extension, $ext_array)){
                return response(['errors' => ['audio' => ['Invalid Audio Format']]], 422);
            }else {
                // Creating the directory, for example, if the date = 18/10/2017, the directory will be 2017/10/
                $directory = 'audio/file/' . date_format($time, 'Y') . '/' . date_format($time, 'm');
                File::ensureDirectoryExists($directory);
                // Creating the file name: random string followed by the day, random number and the hour
                $filename = date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . "." . $extension;
                if ($file->move($directory, $filename)) {
                    $urls = $directory . '/' . $filename;
//                $size = $file->getSize();

                    return response()->json(['message' => 'Uploaded', 'url' => $urls, 'duration' => $duration, 'ext' => $extension], 200);
                }
                return response()->json(['message' => 'Upload Failed'], 422);
            }
        }
    }

    public function uploadCover(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => ['required', 'mimes:jpg,png,bmp', 'max:5125'],
        ], [
            'image.required' => "Please provide your full name.",
            'image.mimes' => "Invalid Image Format",
        ]);
        if($validator->fails()){
            return response(['errors' => $validator->errors()], 422);
        }
        if ($request->hasfile('image')) {
            $time = Carbon::now();
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            // Creating the directory, for example, if the date = 18/10/2017, the directory will be 2017/10/
            $directory = 'audio/cover/'.date_format($time, 'Y') . '/' . date_format($time, 'm');
            File::ensureDirectoryExists($directory);
            // Creating the file name: random string followed by the day, random number and the hour
            $filename = date_format($time, 'd') . rand(1, 9).'-'. \Str::random(9) . date_format($time, 'h') . "." . $extension;
            $location = $directory.'/'.$filename;

            if(Image::make($image)->save($location)){
                return response()->json(['message' => 'Uploaded', 'image' => asset($location), 'imageName' => $location], 200);
            }
            return response()->json(['message' => 'Failed'], 422);
        }
    }
    public function update(Request $request, Audio $audio)
    {
        $validator = Validator::make($request->all(), [
            'caption' => ['required', 'string', 'min:3'],
        ]);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->errors());
        }
        if (is_null($request->audio_file)){
            $audio_file = $audio->file;
        }else{
            $audio_file = $request->audio_file;
        }
        if (is_null($request->cover)){
            $cover = $audio->image;
        }else{
            $cover = $request->cover;
        }
        if (is_null($request->duration)){
            $duration = $audio->duration;
        }else{
            $duration = $request->duration;
        }
        $update = $audio->update(
            [
                'caption' => $request->caption,
                'image' => $cover,
                'file' => $audio_file,
                'status' => $request->status,
                'duration' => $duration,
                'user_id' => \Auth::id(),
            ],
            $request->except('_token','_method'));
        if($update){
            Session::flash('success', "Audio Data Updated Successfully!!");
            return Redirect::back();
        } else {
            Session::flash('error', "Audio Data Update Failed!!");
            return Redirect::back();
        }
    }
    public function formatSeconds( $seconds )
    {
        $hours = 0;
        $milliseconds = str_replace( "0.", '', $seconds - floor( $seconds ) );

        if ( $seconds > 3600 )
        {
            $hours = floor( $seconds / 3600 );
        }
        $seconds = $seconds % 3600;


        return gmdate( 'i:s', $seconds );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Audio  $audio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Audio $audio)
    {
        if ($audio->isOwner()) {

            if (File::exists($audio->file)) {
                File::delete([$audio->file, $audio->image]);
            }
            if ($audio->delete()) {
                Session::flash('success', "Audio File Deleted Successfully!!");
                return Redirect::back();
            } else {
                Session::flash('error', "Audio File Deletion Failed!!");
                return Redirect::back();
            }
        }
        Session::flash('error', "Access Denied!!");
        return Redirect::route('home');
    }
}
