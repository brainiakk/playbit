<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ActivationCode;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $this->guard()->login($user);
        if(\Auth::check()){
            $ul = \Auth::user()->level;

            if ($ul == '1' ) {
                return redirect()->route('portal.admin.home');

            }  else {
                # code...
                // if($request->ajax()){return response()->json(['ul'=> $ul], 200);}
                return redirect()->route('portal.user.home');
            }
        }

    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $code = ActivationCode::active()->whereEmail($data['email'])->first();
        if($code) {
            if($code->code == $data['code']) {
                $user =  User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'level' => '0',
                    'email_verified_at' => now(),
                    'password' => Hash::make($data['password']),
                ]);
                return $user;
            }else{
                return Redirect::back()->with(['error' => 'Oops!! Verification Code Doesn\'t Match']);
            }
        }else{
            return Redirect::back()->with(['error' => 'Oops!! Incorrect or Expired Verification Code.']);
        }
    }
}
