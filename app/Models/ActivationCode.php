<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function scopeActive($scope)
    {
        return $scope->where('expires_at', '>=', now());
    }
}
