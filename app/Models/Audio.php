<?php

namespace App\Models;

use Illuminate\Auth\Access\Gate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $primaryKey = 'audio_id';

    public function scopePending($scope)
    {
        return $scope->where('status', '1');
    }
    public function scopeActive($scope)
    {
        return $scope->where('status', '2');
    }
    public function scopeOwner($scope)
    {
        return $scope->where('user_id', \Auth::id());
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function isOwner()
    {
        if ($this->user_id == \Auth::id()){
            return true;

        }else {
            if (\Auth::user()->level == '1') {
                return true;
            }
            return true;
        }
    }
}
