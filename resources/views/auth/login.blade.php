@extends('layouts.auth')
@section('title', 'Login')
@section('content')
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
    <div class="container aside-xl">
        <a class="navbar-brand block" href="{{ route('home') }}">
            <img src="{{ asset('images/logo-w.png') }}" alt="{{ $appName }}" style="width: 100px; height: auto;">
        </a>
        <section class="m-b-lg">
            <header class="wrapper text-center"><strong>Sign in to get in touch</strong></header>
            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="form-group">
                    <input type="email" name="email" placeholder="Email Address" class="form-control rounded input-lg text-center no-border @error('email') is-invalid @enderror" value="{{ old('email') }}" required />
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" class="form-control rounded input-lg text-center no-border @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" />
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-lg btn-warning lt b-white b-2x btn-block btn-rounded"><i class="icon-arrow-right pull-right"></i><span class="m-r-n-lg">Sign in</span></button>

                <div class="line line-dashed"></div>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a href="{{ route('register') }}" class="btn btn-lg btn-info btn-block rounded">Create an account</a>
            </form>
        </section>
    </div>
</section>
@endsection
