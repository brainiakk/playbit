@extends('layouts.auth')
@section('title', 'Register')
@section('content')
    <section id="content" class="m-t-lg wrapper-md animated fadeInDown">
        <div class="container aside-xl">
            <a class="navbar-brand block" href="{{ route('home') }}">
                <img src="{{ asset('images/logo-w.png') }}" alt="{{ $appName }}" style="width: auto; height: 120px;">
            </a>
            <section class="m-b-lg">
                <header class="wrapper text-center"> <strong>Sign up to find interesting thing</strong> </header>
                <div class="alert @if (Session::has('success')) alert-success @elseif(Session::has('error')) alert-danger @else d-none @endif alert-dismissible show flex items-center mb-2" role="alert">
                    @if(Session::has('success')) <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i>  {{ Session::get('success') }} @elseif(Session::has('error'))  <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> {{ Session::get('error') }}@endif
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button>
                </div>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <input placeholder="Full Name" class="form-control rounded input-lg text-center no-border @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" id="email" placeholder="Email" class="form-control rounded input-lg text-center no-border @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <span>
                            <a href="#" id="veribtn">Send Verification Code</a> <i class='bx bx-loader bx-spin' style="font-size: 15px; vertical-align: middle; display: none; "></i>
                        </span>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Verification Code" class="form-control rounded input-lg text-center no-border" name="code" required/>
                        @error('code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control rounded input-lg text-center no-border @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control rounded input-lg text-center no-border"name="password_confirmation" required autocomplete="new-password">
                    </div>
                    <div class="checkbox i-checks m-b"> <label class="m-l"> <input type="checkbox" checked=""><i></i>
                            Agree the <a href="#">terms and policy</a> </label> </div>
                    <button type="submit" class="btn btn-lg btn-warning lt b-white b-2x btn-block btn-rounded">
                        <i class="icon-arrow-right pull-right"></i><span class="m-r-n-lg">Sign up</span>
                    </button>
                    <div class="line line-dashed"></div>
                    <p class="text-muted text-center"><small>Already have an account?</small></p>
                    <a href="{{ route('login') }}" class="btn btn-lg btn-info btn-block btn-rounded">Sign in</a>
                </form>
            </section>
        </div>
    </section>

@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#veribtn').on('click',function(event){
                $(this).fadeOut();
                $('.bx-spin').show();
                event.preventDefault();
                let _token   = $('meta[name="csrf-token"]').attr('content');


                email = $('#email').val();
                var vurl = '{{ route('verify.code.send') }}';
                $.ajax({
                    url: vurl,
                    type:"POST",
                    data:{
                        "_token": _token,
                        email:email,
                    },
                    success:function(response){
                        $('#veribtn').show();
                        $('.bx-spin').fadeOut();
                        // console.log(response);
                        $.toast({
                            heading: 'Well Done!',
                            text: response.msg,
                            icon: 'success',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#5ba035',  // To change the background
                            position: 'top-right',
                            icon: 'success',
                        });
                    },
                    error: function(data) {
                        $('#veribtn').show();
                        $('.bx-spin').fadeOut();
                        $.toast({
                            heading: 'Oh snap!',
                            text: data.responseJSON.msg,
                            icon: 'error',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#bf441d',  // To change the background
                            position: 'top-right',
                            icon: 'error',
                        });
                    }
                });
            });
        });
    </script>
@endsection
