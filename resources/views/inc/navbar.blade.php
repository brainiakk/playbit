<header class="bg-white-only header header-md navbar navbar-fixed-top-xs">
    <div class="navbar-header aside bg-info nav-xs">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html"> <i class="icon-list"></i> </a>
        <a href="{{ route('home') }}" class="navbar-brand text-lt"> <i class="icon-earphones"></i> <img src="{{ asset('images/logo.png') }}" alt="." class="hide" /> <span class="hidden-nav-xs m-l-sm">PlayBit</span> </a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user"> <i class="icon-settings"></i> </a>
    </div>
    <ul class="nav navbar-nav hidden-xs">
        <li>
            <a href="#nav,.navbar-header" data-toggle="class:nav-xs,nav-xs" class="text-muted"> <i class="fa fa-indent text"></i> <i class="fa fa-dedent text-active"></i> </a>
        </li>
    </ul>
    <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
        <div class="form-group">
            <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-sm bg-white btn-icon rounded"><i class="fa fa-search"></i></button>
                            </span>
                <input type="text" class="form-control input-sm no-border rounded" placeholder="Search songs, albums..." />
            </div>
        </div>
    </form>
    <div class="navbar-right">
        <ul class="nav navbar-nav m-n hidden-xs nav-user user">
            @auth()
            <li class="dropdown">
                <a href="#" class="dropdown-toggle bg clear" data-toggle="dropdown">
                    <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm"> <img src="{{ asset('dist/images/profile-6.jpg') }}" alt="..." /> </span>
                    {{ \Auth::user()->name }}<b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInRight">

                    <li><a href="@can('admin') {{ route('portal.admin.home') }} @else {{ route('portal.user.home') }} @endcan">Dashboard</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="ajaxModal">Logout</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>

                </ul>
            </li>
            @else
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            @endauth
        </ul>
    </div>
</header>
