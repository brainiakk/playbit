<script src="{{ asset('js/app.v1.js') }}"></script>
<script src="{{ asset('js/app.plugin.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jPlayer/jquery.jplayer.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jPlayer/add-on/jplayer.playlist.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jPlayer/demo.js') }}"></script>
<script src="{{ asset('js/boxicons.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/jquery-toast.min.js') }}"></script>
@yield('scripts')
