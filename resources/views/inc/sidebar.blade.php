
<!-- .aside -->
<aside class="bg-black dk nav-xs aside hidden-print" id="nav">
    <section class="vbox">
        <section class="w-f-md scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railopacity="0.2">
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                    <ul class="nav bg clearfix">
                        <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">Discover</li>
                        <li>
                            <a href="{{ route('home') }}"> <i class="icon-disc icon text-success"></i> <span class="font-bold">All Audio</span> </a>
                        </li>
                    </ul>
                </nav>
                <!-- / nav -->
            </div>
        </section>
        <footer class="footer hidden-xs no-padder text-center-nav-xs">
            <div class="bg hidden-xs">
                <div class="dropdown dropup wrapper-sm clearfix">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="thumb-sm avatar pull-left m-l-xs"> <img src="{{ asset('dist/images/profile-6.jpg') }}" class="dker" alt="..." /> <i class="on b-black"></i> </span>
                        <span class="hidden-nav-xs clear">
                                                <span class="block m-l"> <strong class="font-bold text-lt">John.Smith</strong> <b class="caret"></b> </span> <span class="text-muted text-xs block m-l">Art Director</span>
                                            </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight aside text-left">

                        <li><a href="@can('admin') {{ route('portal.admin.home') }} @else {{ route('portal.user.home') }} @endcan">Dashboard</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-formm').submit();" data-toggle="ajaxModal">Logout</a>

                            <form id="logout-formm" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    </section>
</aside>
<!-- /.aside -->
