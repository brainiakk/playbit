
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('js/jPlayer/jplayer.flat.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/app.v1.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/jquery-toast.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/boxicons.css') }}" />
<!--[if lt IE 9]>
    <script src="{{ asset('js/ie/html5shiv.js') }}"></script>
    <script src="{{ asset('js/ie/respond.min.js') }}"></script>
    <script src="{{ asset('js/ie/excanvas.js') }}"></script>
<![endif]-->
