<!DOCTYPE html>
<html lang="en" class="app">

<head>
    <meta charset="utf-8">
    <title> @yield('title') | {{ $appName }} - {{ $appTitle }}</title>
    @include('inc.styles')
</head>


<body class="bg-info">
@yield('content')
<!-- footer -->
<footer id="footer">
    <div class="text-center padder">
        <p>
            <small>
                {{ $appName }} by {{ $appCr }}<br />
                &copy; {{ date('Y') }}
            </small>
        </p>
    </div>
</footer>
    @include('inc.scripts')
</body>

</html>
