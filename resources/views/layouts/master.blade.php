<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>@yield('title') | {{ $appName }} - {{ $appTitle }}</title>
    @include('inc.styles')
</head>

<body class="">
<section class="vbox">
    @include('inc.navbar')
    <section>
        <section class="hbox stretch">
            @include('inc.sidebar')
            @yield('content')
        </section>
    </section>
</section>
<!-- Bootstrap -->
<!-- App -->
@include('inc.scripts')
</body>
</html>
