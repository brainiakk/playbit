<!DOCTYPE html>
<html lang="en" class="light">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - {{ $appName }}</title>
    @include('inc.portal.styles')
<!-- END: CSS Assets-->
</head>

<!-- END: Head -->
<body class="main">
@include('inc.portal.msidebar')
<div class="flex">
@include('inc.portal.sidebar')
<!-- BEGIN: Content -->
    <div class="content">
        @include('inc.portal.navbar')
        @yield('content')
    </div>
    <!-- END: Content -->
</div>

<!-- END: Dark Mode Switcher-->
@include('inc.portal.scripts')
</body>
</html>
