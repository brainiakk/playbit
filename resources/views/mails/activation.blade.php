<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <style>
        body{
            background-image: url("{{ asset('images/email-bg.jpeg') }}");
            background-position: top;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: contain;
        }
        .body-wrapper{
            width: 100%;
            height: 100%;
            background: inherit;
            margin: 30px auto;
        }
        .content-wrapper{
            width: 500px;
            height: 600px;
            background: #fff;
            margin: 0 auto;
        }
        .content{
            padding: 25px;
        }
        .verify-btn{
            text-decoration: none;
            color: #fff;
            background: #1D89CF;
            padding: 15px 20px;
            text-align: center;
            font-family: 'Poppins', sans-serif;
        }
        .email-footer{
            margin: 30px auto;
            text-align: center;
            font: normal normal 10px 'Poppins', sans-serif;

        }
        .btn-container{
            display: block;
            margin: 0 auto;
            width: auto;
        }

        .btn-container a{
            display: block;
        }

    </style>
</head>
<body>
<div class="body-wrapper">
    <div class="content-wrapper">
        <h4 style="background-color: #4925C3; width: 100%; text-align: center; padding: 15px 0; color: #fff; text-transform: capitalize; font-family: 'Raleway', sans-serif;">
            Email Verification Code
        </h4>

        <div class="content">
            <img src="https://ucarecdn.com/720a6090-9955-4f46-9e7d-67bec9439790/logo2.png" style='margin: 0 auto; display: block; width: 120px; height: auto;'><br/>
            <h3 style="font-weight: normal; font-family: 'Josefin Slab', serif; text-align: center; font-size: 30px;">
                Hi,
            </h3>
            <h5 style="font-family: 'Raleway', sans-serif; text-align: center; font-size: 18px; font-weight: normal;">
                You requested an email verification code
            </h5>

            <p style="font-family: 'Raleway', sans-serif; font-size: 12px; text-align: center;">

                <h1 style="text-align: center;"><b>{{ $code }}</b></h1>


            </p><br/>


            <div class="email-footer">
                If you did not request for the code above on <a href="{{ env('APP_URL') }}"> {{ $appName }} </a> please ignore this email, Thank you.<br/>
            &copy; Copyright {{ date('Y') }}&nbsp;{{ $appName }} <br/>
            </div>
        </div>
    </div>

</div>

</body>
</html>
