@extends('layouts.master')
@section('title', 'Home')
@section('content')
    <section id="content">
        <section class="hbox stretch">
            <section>
                <section class="vbox">
                    <section class="scrollable padder-lg w-f-md" id="bjax-target">
                        <a href="{{ route('home') }}" class="pull-right text-muted m-t-lg" data-toggle="class:fa-spin"><i class="icon-refresh i-lg inline" id="refresh"></i></a>
                        <h2 class="font-thin m-b">
                            Discover
                            <span class="musicbar animate inline m-l-sm" style="width: 20px; height: 20px;">
                                                <span class="bar1 a1 bg-primary lter"></span> <span class="bar2 a2 bg-info lt"></span> <span class="bar3 a3 bg-success"></span> <span class="bar4 a4 bg-warning dk"></span>
                                                <span class="bar5 a5 bg-danger dker"></span>
                                            </span>
                        </h2>
                        <div class="row row-sm">
                            @forelse($audios as $audio)
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <div class="item">
                                        <div class="pos-rlt" style="height: 300px !important; width: 100% !important;">
                                            <div class="bottom"><span class="badge bg-info m-l-sm m-b-sm">{{ $audio->duration }}</span></div>
                                            <div class="item-overlay opacity r r-2x bg-black">
                                                <div class="text-info padder m-t-sm text-sm">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o text-muted"></i>
                                                </div>
                                                <div class="center text-center m-t-n">
                                                    <a href="#" data-song="{{ $loop->index }}" class="play_new" ><i class="icon-control-play i-2x"></i></a>
                                                </div>
                                                <div class="bottom padder m-b-sm">
                                                    <a href="#" class="pull-right" onclick="event.preventDefault(); document.getElementById('download-form{{$audio->audio_id}}').submit();"> <i class="fa fa-cloud-download"></i>

                                                        <form id="download-form{{$audio->audio_id}}" action="{{ route('download', $audio->audio_id) }}" method="GET" class="d-none">
                                                            @csrf
                                                        </form>
                                                    </a> <a href="#"> <i class="fa fa-plus-circle"></i> </a>
                                                </div>
                                            </div>
                                            <a href="#"><img style="width: 100%; height: 100%; object-fit: cover;" src="{{ asset($audio->image) }}" alt="" class="r r-2x img-full" /></a>
                                        </div>
                                        <div class="padder-v"><a href="#" class="text-ellipsis">{{ \Str::limit($audio->caption, 20) }}</a>
                                            <a href="#" class="text-ellipsis text-xs text-muted">by {{ $audio->user->name }}</a></div>
                                    </div>
                                </div>
                            @empty
                                <div class="alert alert-warning">
                                    No Audio File Found Yet...Visit us later for more
                                </div>

                            @endforelse
                        </div>
                    </section>
                    <footer class="footer bg-dark">
                        <div id="jp_container_N">
                            <div class="jp-type-playlist">
                                <div id="jplayer_N" class="jp-jplayer hide"></div>
                                <div class="jp-gui">
                                    <div class="jp-video-play hide"><a class="jp-video-play-icon">play</a></div>
                                    <div class="jp-interface">
                                        <div class="jp-controls">
                                            <div>
                                                <a class="jp-previous"><i class="icon-control-rewind i-lg"></i></a>
                                            </div>
                                            <div>
                                                <a class="jp-play"><i class="icon-control-play i-2x"></i></a> <a class="jp-pause hid"><i class="icon-control-pause i-2x"></i></a>
                                            </div>
                                            <div>
                                                <a class="jp-next"><i class="icon-control-forward i-lg"></i></a>
                                            </div>
                                            <div class="hide">
                                                <a class="jp-stop"><i class="fa fa-stop"></i></a>
                                            </div>
                                            <div>
                                                <a class="" data-toggle="dropdown" data-target="#playlist"><i class="icon-list"></i></a>
                                            </div>
                                            <div class="jp-progress hidden-xs">
                                                <div class="jp-seek-bar dk">
                                                    <div class="jp-play-bar bg-info"></div>
                                                    <div class="jp-title text-lt">
                                                        <ul>
                                                            <li></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hidden-xs hidden-sm jp-current-time text-xs text-muted"></div>
                                            <div class="hidden-xs hidden-sm jp-duration text-xs text-muted"></div>
                                            <div class="hidden-xs hidden-sm">
                                                <a class="jp-mute" title="mute"><i class="icon-volume-2"></i></a> <a class="jp-unmute hid" title="unmute"><i class="icon-volume-off"></i></a>
                                            </div>
                                            <div class="hidden-xs hidden-sm jp-volume">
                                                <div class="jp-volume-bar dk">
                                                    <div class="jp-volume-bar-value lter"></div>
                                                </div>
                                            </div>
                                            <div>
                                                <a class="jp-shuffle" title="shuffle"><i class="icon-shuffle text-muted"></i></a> <a class="jp-shuffle-off hid" title="shuffle off"><i class="icon-shuffle text-lt"></i></a>
                                            </div>
                                            <div>
                                                <a class="jp-repeat" title="repeat"><i class="icon-loop text-muted"></i></a> <a class="jp-repeat-off hid" title="repeat off"><i class="icon-loop text-lt"></i></a>
                                            </div>
                                            <div class="hide">
                                                <a class="jp-full-screen" title="full screen"><i class="fa fa-expand"></i></a> <a class="jp-restore-screen" title="restore screen"><i class="fa fa-compress text-lt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jp-playlist dropup" id="playlist">
                                    <ul class="dropdown-menu aside-xl dker">
                                        <!-- The method Playlist.displayPlaylist() uses this unordered list -->
                                        <li class="list-group-item"></li>
                                    </ul>
                                </div>
                                <div class="jp-no-solution hide">
                                    <span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your
                                    <a href="../../products/flashplayer/end-of-life-alternative.html" target="_blank">Flash plugin</a>.
                                </div>
                            </div>
                        </div>
                    </footer>
                </section>
            </section>
        </section>
        <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
    </section>
@endsection
@section('scripts')

    <script type="text/javascript" >

        $(document).ready(function(){
            var url = '{{ route("all.audios") }}';
            var audios;
            var myPlaylist;
            $.ajax({
                url: url,
                type: 'get',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                cache:false,
                contentType: 'json',
                processData: false,
                success: function(data){
                    console.log(data);
                    audios = data;
                    var audioo = [];
                    var datas = [];
                       audios.forEach((audio) => {
                           audioo.push(
                                   {
                                       'title': audio.caption,
                                       'artist': audio.user.name,
                                       'mp3' : audio.file,
                                       'oga': audio.file,
                                       'wav': audio.file,
                                       'poster': audio.image
                                   })

                        })
                    console.log(audioo)
                    myPlaylist = new jPlayerPlaylist({
                        jPlayer: "#jplayer_N",
                        cssSelectorAncestor: "#jp_container_N"
                    }, audioo, {
                        playlistOptions: {
                            enableRemoveControls: true,
                            autoPlay: true
                        },
                        swfPath: "js/jPlayer",
                        supplied: "webmv, ogv, m4v, oga, mp3",
                        smoothPlayBar: true,
                        keyEnabled: true,
                        audioFullScreen: false
                    });

                }

            });
            $('.play_new').click(function (e) {
                e.preventDefault();
                // alert($(this).data("song"))
                myPlaylist.play($(this).data("song"))
            })
            $(document).on($.jPlayer.event.pause, myPlaylist.cssSelector.jPlayer,  function(){
                $('.musicbar').removeClass('animate');
                $('.jp-play-me').removeClass('active');
                $('.jp-play-me').parent('li').removeClass('active');
            });

            $(document).on($.jPlayer.event.play, myPlaylist.cssSelector.jPlayer,  function(){
                $('.musicbar').addClass('animate');
            }); $(document).on('click', '.jp-play-me', function(e){
                e && e.preventDefault();
                var $this = $(e.target);
                if (!$this.is('a')) $this = $this.closest('a');

                $('.jp-play-me').not($this).removeClass('active');
                $('.jp-play-me').parent('li').not($this.parent('li')).removeClass('active');

                $this.toggleClass('active');
                $this.parent('li').toggleClass('active');
                if( !$this.hasClass('active') ){
                    myPlaylist.pause();
                }else{
                    var i = Math.floor(Math.random() * (1 + 7 - 1));
                    myPlaylist.play(i);
                }

            });



        });
    </script>
@endsection
