@extends('portal.admin.layout')
@section('title', $provider->name)
@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <form class="intro-y col-span-12 lg:col-span-8" method="post" action="{{ route('portal.admin.provider.update', $provider->provider_id) }}">
        <div class="alert @if (Session::has('success')) alert-success @elseif(Session::has('error')) alert-danger @else d-none @endif alert-dismissible show flex items-center mb-2" role="alert">
            @if(Session::has('success')) <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i>@elseif(Session::has('error'))  <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> @endif {{ Session::get('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button>
        </div>
        @csrf()
        @method('PATCH')
        <!-- BEGIN: Form Layout -->
        <div class="intro-y box p-5">
            <div class="my-5">
                <label for="crud-form-1" class="form-label">Title</label>
                <input id="crud-form-1" type="text" value="{{ old('title', $user->provider_title) }}" class="form-control w-full" name="title" placeholder="Client's Title">
            </div>
            <div class="sm:grid grid-cols-2 gap-2 my-5">
                <div class="">
                    <label for="crud-form-1" class="form-label">First Name</label>
                    <input id="crud-form-1" value="{{ old('fname', $user->fname) }}" type="text" class="form-control w-full" name="fname" placeholder="John">
                </div>
                <div class="">
                    <label for="crud-form-1" class="form-label">Last Name</label>
                    <input id="crud-form-1" value="{{ old('lname', $user->lname) }}" type="text" class="form-control w-full" name="lname" placeholder="Doe">
                </div>
            </div>
            <div class="my-5">
                <label for="crud-form-1" class="form-label">Username</label>
                <input id="crud-form-1" value="{{ old('username', $user->username) }}" type="text" class="form-control w-full" name="username" placeholder="johndoe">
            </div>
            <div class="sm:grid grid-cols-2 gap-2 my-5">
                <div class="">
                    <label for="crud-form-1" class="form-label">Email Address</label>
                    <input id="crud-form-1" value="{{ old('email', $user->email) }}" type="email" class="form-control w-full" name="email" placeholder="johndoe@example.com">
                </div>
                <div class="">
                    <label for="crud-form-1" class="form-label">Phone Number</label>
                    <input id="crud-form-1" type="tel" value="{{ old('tel', $user->tel) }}" class="form-control w-full" name="tel" placeholder="+234808989009">
                </div>
            </div>
            <div class="my-5">
                <label for="crud-form-1" class="form-label">Name of Institution</label>
                <input id="crud-form-1" type="text" value="{{ old('name', $provider->name) }}" class="form-control w-full" name="institution" placeholder="Harvard University">
            </div>
            <div class="my-5">
                <label for="crud-form-1" class="form-label">Position</label>
                <input id="crud-form-1" value="{{ old('role', $user->provider_role) }}" type="text" class="form-control w-full" name="position" placeholder="COO">
            </div>
            <div class="my-5">
                <label for="crud-form-1" class="form-label">Sub Expiry Date</label>
                <input id="crud-form-1" type="text" value="{{ old('expires_at', $provider->expires_at->format('d M, Y')) }}"  class="form-control w-full datepicker" data-single-mode="true" name="expires_at" placeholder="12 Mar, 2021">
            </div>
            <div class="mt-3">
                <label>Active Status</label>
                <div class="mt-2">
                    <input type="checkbox" class="form-check-switch" value="2" name="status" @if($provider->status > 1) checked @endif>
                </div>
            </div>
            <div class="text-right mt-5">
                <button type="button" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                <button type="submit" class="btn btn-primary w-24">Save</button>
            </div>
        </div>
        <!-- END: Form Layout -->
    </form>
</div>
@endsection
