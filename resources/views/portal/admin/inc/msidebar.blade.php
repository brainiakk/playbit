<!-- BEGIN: Mobile Menu -->
<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="" class="flex mr-auto">
            <img alt="{{ $appName }}" class="w-50" style="width: 30%;" src="{{ asset('images/logo-w.png') }}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-29 py-5 hidden">
        <li>
            <a href="{{ route('portal.admin.home') }}" class="menu <?php if(isset($page) && $page == 'home'){ echo 'menu--active'; } ?>">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.audio.home') }}" class="menu @if(\Route::current()->getName() == 'portal.admin.audio.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="upload"></i> </div>
                <div class="menu__title">  All Audio </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.audio.create') }}" class="menu @if(\Route::current()->getName() == 'portal.admin.audio.create') menu--active @endif">
                <div class="menu__icon"> <i data-feather="upload"></i> </div>
                <div class="menu__title">  Upload Audio </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.audio.pending') }}" class="menu @if(\Route::current()->getName() == 'portal.admin.audio.pending') menu--active @endif">
                <div class="menu__icon"> <i data-feather="headphones"></i> </div>
                <div class="menu__title"> Pending Audio </div>
            </a>
        </li>
    </ul>
</div>
<!-- END: Mobile Menu -->
