<!-- BEGIN: Side Menu -->
<nav class="side-nav">
    <a href="{{ route('portal.admin.home') }}" class="intro-x flex items-center pl-5 pt-4">
        <img alt="{{ $appName }}" class="w-50" style="width: 70%;" src="{{ asset('images/logo-w.png') }}">
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="{{ route('portal.admin.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.admin.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                <div class="side-menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.audio.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.admin.audio.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="play-circle"></i> </div>
                <div class="side-menu__title">  All Audio </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.audio.create') }}" class="side-menu @if(\Route::current()->getName() == 'portal.admin.audio.create') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="upload"></i> </div>
                <div class="side-menu__title">  Upload Audio </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.audio.pending') }}" class="side-menu @if(\Route::current()->getName() == 'portal.admin.audio.pending') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="headphones"></i> </div>
                <div class="side-menu__title"> Pending Audio </div>
            </a>
        </li>

    </ul>
</nav>
<!-- END: Side Menu -->
