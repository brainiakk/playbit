@extends('portal.user.layout')
@section('title', 'Upload Audio')
@section('content')
    @if($audio->audio_id)
        <form class="intro-y col-span-12 lg:col-span-8" method="post" action="{{ route('portal.user.audio.update', $audio->audio_id) }}">
            @method('patch')
            @else
                <form class="intro-y col-span-12 lg:col-span-8" method="post" action="{{ route('portal.user.audio.store') }}" enctype="multipart/form-data">
                @endif
                @include('portal.user.inc.alert')
                @csrf()
                <!-- BEGIN: Form Layout -->
                    <div class="intro-y box p-5">
                        <div class="my-5">
                            <label for="caption" class="form-label">Caption <b class="text-theme-6">*</b></label>
                            <textarea id="caption" rows="3" class="form-control w-full" name="caption" placeholder="Caption" required>
                                {{ old('caption', $audio->caption) }}
                            </textarea>
                            @error('caption')
                            <span class="text-theme-6 mt-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="my-5">
                            <label for="crud-form-1" class="form-label">Select Audio Cover Image <b class="text-theme-6">*</b></label>

                                <input name="image" type="file" id="imageFile"  class="form-control w-full"/>
                            <input type="hidden" name="cover" id="coverimage" value="{{ old('image', $audio->image) }}">
                            @error('cover')
                            <span class="text-theme-6 mt-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <small class="text-theme-6 font-bold">Supported Image formats: jpg,png,bmp are allowed and not above 5MB in size</small>
                        </div>
                        <div class="form-group mt-4">
                            <div class="progress h-4 rounded" style="display: none;" id="progress">
                                <div class="progress-bar bg-theme-6" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" ></div>
                            </div>
                        </div>
                        <div class="rounded mt-5 p-1" id="img-display" style="height: 180px; width: 180px; background-color: #F1F5F8; display: none !important;  "></div>
                        <div class="my-5">
                            <label for="crud-form-1" class="form-label">Select Audio File <b class="text-theme-6">*</b></label>

                                <input name="audio" type="file" id="audioFile"  class="form-control w-full" accept="audio/*"/>
                            <input type="hidden" name="audio_file" id="audio" required>
                            <input type="hidden" name="duration" value="{{ old('duration', $audio->duration) }}" id="duration" required>
                            @error('audio_file')
                            <span class="text-theme-6 mt-2" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <small class="text-theme-6 font-bold">Only Audio formats: mp3,ogg,wav are allowed and not above 10MB in size</small>
                        </div>
                        <div class="form-group mt-4">
                            <div class="progress h-4 rounded" style="display: none;" id="progress2">
                                <div class="progress-bar bg-theme-6" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" ></div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <label>Publish to Admin</label>
                            <div class="mt-2">
                                <input type="checkbox" class="form-check-switch" @if(old('status', $audio->status) == '1') checked @endif value="1" name="status">
                            </div>
                        </div>
                        <div class="text-right mt-5">
                            <button type="button" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                            <button type="submit" class="btn btn-primary w-24">Save</button>
                        </div>
                    </div>
                    <!-- END: Form Layout -->
                </form>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){

        @if(!$audio->audio_id)
        $('#caption').empty();
        @endif

        $('input[name=image]').change(function(e){
            // var image = $(this).files[0];
            var formData = new FormData(); // Currently empty
            var _token = '{{ csrf_token() }}';
            formData.append('image', $(this)[0].files[0]);
            var url = '{{ route("upload.cover") }}';
            $('#progress').show();
            $("#progress").css("display", "block");

            $('.progress-bar').css('width',"0%");
            $('.progress-bar').html("0%");
            $.ajax({
                url: url,
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSubmit: function () {
                },
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.progress-bar').css('width',percentComplete+"%");
                            $('.progress-bar').html(percentComplete+"%");
                            if (percentComplete > 29 && percentComplete < 60) {
                                $('.progress-bar').removeClass('bg-theme-6');
                                $('.progress-bar').addClass('bg-theme-12');
                            }else if (percentComplete > 59 && percentComplete < 100) {
                                $('.progress-bar').removeClass('bg-theme-12');
                                $('.progress-bar').addClass('bg-theme-1');
                            }else if (percentComplete === 100) {
                                $('.progress-bar').removeClass('bg-theme-12');
                                $('.progress-bar').addClass('bg-theme-9');
                            }
                        }
                    }, false);
                    return xhr;
                },
                success: function(data){
                    $('#progress').fadeOut();
                    $('#coverimage').val(data.imageName);
                    $('#img-display').show();
                    $('#img-display').html('<img style="width: 100%; height: 100%; object-fit: cover; vertical-align: middle;" src='+data.image+'>');
                    // console.log(data.message);

                }
            });
        });
    });

</script>
<script>
    $(document).ready(function(){
        $('input[name=audio]').change(function(e){
            // var image = $(this).files[0];
            var formData = new FormData(); // Currently empty
            var _token = '{{ csrf_token() }}';
            formData.append('audio', $(this)[0].files[0]);
            var auurl = '{{ route("upload.audio") }}';
            $('#progress2').show();
            $("#progress2").css("display", "block");
            $('.progress-bar').css('width',"0%");
            $('.progress-bar').html("0%");
            $.ajax({
                url: auurl,
                type: 'post',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                beforeSubmit: function () {
                },
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.progress-bar').css('width',percentComplete+"%");
                            $('.progress-bar').html(percentComplete+"%");
                            if (percentComplete > 29 && percentComplete < 60) {
                                $('.progress-bar').removeClass('bg-theme-6');
                                $('.progress-bar').addClass('bg-theme-12');
                            }else if (percentComplete > 59 && percentComplete < 100) {
                                $('.progress-bar').removeClass('bg-theme-12');
                                $('.progress-bar').addClass('bg-theme-1');
                            }else if (percentComplete === 100) {
                                $('.progress-bar').removeClass('bg-theme-12');
                                $('.progress-bar').addClass('bg-theme-9');
                            }
                        }
                    }, false);
                    return xhr;
                },
                success: function(data){
                    $('#progress2').fadeOut();
                    $('#audio').val(data.url);
                    $('#duration').val(data.duration);
                    $('#fext').val(data.ext);
                    $('#fsize').val(data.size);

                },
                error: function(e){
                    if(e.status == 422){

                        $.toast({
                            heading: 'Oh snap!',
                            text: e.responseJSON.errors.audio[0],
                            icon: 'error',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#bf441d',  // To change the background
                            position: 'top-right',
                            icon: 'error',
                        });
                    }
                    $.toast({
                        heading: 'Oh snap!',
                        text: 'Oops!! Something went wrong',
                        icon: 'error',
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#bf441d',  // To change the background
                        position: 'top-right',
                        icon: 'error',
                    });
                }
            });
        });
    });

</script>
@endsection
