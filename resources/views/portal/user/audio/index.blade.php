@extends('portal.user.layout')
@section('title', 'All Audio Shared')
@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('portal.user.audio.create') }}" class="btn btn-primary shadow-md mr-2">Upload New Audio</a>
        <div class="dropdown">
            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
            </button>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing @if($audios->total() < 2) 1 @else 1 to {{ $audios->total() }} @endif of {{ $audios->total() }} entries</div>

    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        @if (count($audios) > 0)
            <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th class="whitespace-nowrap">Cover</th>
                <th class="whitespace-nowrap">Caption</th>
                <th class="text-center whitespace-nowrap">Created By</th>
                <th class="text-center whitespace-nowrap">Status</th>
                <th class="text-center whitespace-nowrap">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($audios as $audio)

            <tr class="intro-x">
                <td class="w-40">
                    <div class="flex">
                        <div class="w-10 h-10 image-fit zoom-in">
                            <img alt="{{ \Str::limit(\Str::slug($audio->caption), 10) }}" class="tooltip rounded-full" src="{{ asset($audio->image) }}" title="{{ \Str::limit($audio->caption, 10) }}">
                        </div>
                    </div>
                </td>
                <td>
                    <a href="" class="font-medium whitespace-nowrap">{{ \Str::limit($audio->caption, 10) }}</a>
                </td>
                <td class="text-center">{{ $audio->user->name }}</td>
                <td class="w-40">
                    @if($audio->status == '0')
                        <span class="text-theme-6"> Draft</span>
                    @elseif($audio->status == '1')
                        <span class="text-theme-12"> Pending</span>
                    @elseif($audio->status == '2')
                        <span class="text-theme-9"> Published</span>
                    @endif
                </td>
                <td class="table-report__action w-56">
                    <div class="flex justify-center items-center">
                        <a class="flex items-center mr-3" href="{{ route('portal.user.audio.edit', $audio->audio_id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                        <a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{ $audio->audio_id }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                    </div>
                </td>
            </tr>
            @push('audio-delete-modal')
                @include('portal.user.inc.audio-delete-modal', ['audio' => $audio])
            @endpush
            @endforeach

            </tbody>
        </table>
        @else
            <div class="col-12 alert alert-danger"><i data-feather="alert-octagon"></i> Oops!! No Audio Record Found.</div>
        @endif
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
        <ul class="pagination">
            {!! $audios->links() !!}
        </ul>
    </div>
    <!-- END: Pagination -->
</div>

@endsection
@section('scripts')
    <script>
        $('.item').on('change', function () {
            alert('');
            var val = $(this).val();
            window.location = window.location.href+'?items='+val;
        });
    </script>
@endsection
