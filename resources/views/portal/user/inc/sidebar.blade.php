<!-- BEGIN: Side Menu -->
<nav class="side-nav">
    <a href="{{ route('portal.user.home') }}" class="intro-x flex items-center pl-5 pt-4">
        <img alt="{{ $appName }}" class="w-50" style="width: 70%;" src="{{ asset('images/logo-w.png') }}">
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="{{ route('portal.user.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.user.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                <div class="side-menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.user.audio.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.user.audio.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="play-circle"></i> </div>
                <div class="side-menu__title">  All Audio </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.user.audio.create') }}" class="side-menu @if(\Route::current()->getName() == 'portal.user.audio.create') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="upload"></i> </div>
                <div class="side-menu__title">  Upload Audio </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.user.audio.pending') }}" class="side-menu @if(\Route::current()->getName() == 'portal.user.audio.pending') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="headphones"></i> </div>
                <div class="side-menu__title"> Pending Audio </div>
            </a>
        </li>

    </ul>
</nav>
<!-- END: Side Menu -->
