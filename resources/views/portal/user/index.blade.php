@extends('portal.user.layout')
@section('title', 'Dashboard')
@section('content')
<div class="grid grid-cols-12 gap-6">
<div class="col-span-12 xxl:col-span-9">
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: General Report -->
        <div class="col-span-12 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                    General Report
                </h2>
                <a href="" class="ml-auto flex text-theme-1 dark:text-theme-10"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Reload Data </a>
            </div>
            <div class="grid grid-cols-12 gap-6 mt-5">
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">

                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="headphones" class="report-box__icon text-theme-11"></i>
                                <div class="ml-auto">
{{--                                    <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="33% Higher than last month"> 33% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>--}}
                                </div>
                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6">{{ \App\Models\Audio::owner()->count() }}</div>
                            <div class="text-base text-gray-600 mt-1">My Audios</div>
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <div class="report-box zoom-in">
                        <div class="box p-5">
                            <div class="flex">
                                <i data-feather="play-circle" class="report-box__icon text-theme-19"></i>
                                <div class="ml-auto">
{{--                                    <div class="report-box__indicator bg-theme-6 tooltip cursor-pointer" title="2% Lower than last month"> 2% <i data-feather="chevron-down" class="w-4 h-4 ml-0.5"></i> </div>--}}
                                </div>
                            </div>
                            <div class="text-3xl font-bold leading-8 mt-6">{{ \App\Models\Audio::owner()->active()->count() }}</div>
                            <div class="text-base text-gray-600 mt-1">Published Audios</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
