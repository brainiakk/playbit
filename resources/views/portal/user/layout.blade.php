<!DOCTYPE html>
<html lang="en" class="light">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - {{ $appName }}</title>
@include('portal.user.inc.styles')
<!-- END: CSS Assets-->
</head>

<!-- END: Head -->
<body class="main">
@include('portal.user.inc.msidebar')
<div class="flex">
@include('portal.user.inc.sidebar')
<!-- BEGIN: Content -->
    <div class="content">
        @include('portal.user.inc.navbar')
        @yield('content')
    </div>
    <!-- END: Content -->
</div>

@stack('audio-delete-modal')
<!-- END: Dark Mode Switcher-->
@include('portal.user.inc.scripts')
@yield('scripts')
</body>
</html>
