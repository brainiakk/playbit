<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'AudioController@home')->name('home');
//    ->middleware('auth');

Route::get('/download/{audio}', 'AudioController@getDownload')->name('download');
Route::post('/send-code', 'ActivationCodeController@sendCode')->name('verify.code.send');
Auth::routes();

Route::prefix('portal')->name('portal.')->middleware('auth')->group(function () {
    Route::prefix('user')->name('user.')->group(function () {
        Route::get('/', 'AudioController@index')->name('home');
        Route::prefix('audio')->name('audio.')->group(function () {
            Route::get('/', 'AudioController@all')->name('home');
            Route::get('/create', 'AudioController@create')->name('create');
            Route::get('/pending', 'AudioController@pending')->name('pending');
            Route::get('/edit/{audio}', 'AudioController@edit')->name('edit');
            Route::post('/store', 'AudioController@store')->name('store');
            Route::patch('/update/{audio}', 'AudioController@update')->name('update');
            Route::get('/delete/{audio}', 'AudioController@destroy')->name('delete');
        });
    });
    Route::prefix('admin')->name('admin.')->middleware('admin')->group(function () {
        Route::get('/', 'AdminController@index')->name('home');
        Route::prefix('audio')->name('audio.')->group(function () {
            Route::get('/', 'AdminController@allAudio')->name('home');
            Route::get('/create', 'AdminController@create')->name('create');
            Route::get('/pending', 'AdminController@pending')->name('pending');
            Route::get('/edit/{audio}', 'AdminController@edit')->name('edit');
            Route::post('/store', 'AudioController@store')->name('store');
            Route::patch('/update/{audio}', 'AudioController@update')->name('update');
            Route::get('/delete/{audio}', 'AudioController@destroy')->name('delete');
            Route::get('/publish/{audio}', 'AdminController@publish')->name('publish');
            Route::get('/reject/{audio}', 'AdminController@reject')->name('reject');
        });
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
